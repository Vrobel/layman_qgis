<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="cs_CZ" sourcelanguage="en">
<context>
    <name>DialogBase</name>
    <message>
        <location filename="../dlg_addLayer.ui" line="35"/>
        <source>Layman - Layers</source>
        <translation>Layman - Vrstvy</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="47"/>
        <source>Load WMS</source>
        <translation>Načíst WMS</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="64"/>
        <source>Layers on Layman</source>
        <translation type="obsolete">Vrstvy v Laymanu</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="64"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="114"/>
        <source>Preview:</source>
        <translation>Náhled:</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="130"/>
        <source>More info</source>
        <translation>Více info</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="143"/>
        <source>Load WFS</source>
        <translation>Načíst WFS</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="156"/>
        <source>Delete layer</source>
        <translation>Smazat vrstvu</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="29"/>
        <source>Layman</source>
        <translation>Layman</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="47"/>
        <source>Login</source>
        <translation>Přihlásit se</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="73"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="86"/>
        <source>Login:</source>
        <translation>Email:</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="93"/>
        <source>Server:</source>
        <translation>Server:</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="35"/>
        <source>Layman - Create Composite</source>
        <translation>Layman - Vytvořit mapovou kompozici</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="47"/>
        <source>Create </source>
        <translation>Vytvořit</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="60"/>
        <source>Storno</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="276"/>
        <source>Layer</source>
        <translation>Vrstva</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="88"/>
        <source>You can choose externt of composition from layer:</source>
        <translation>Můžete zvolit prostorový rozsah z vrstvy:</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="101"/>
        <source>Composition name:</source>
        <translation>Jméno kompozice:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="281"/>
        <source>Title:</source>
        <translation>Název:</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="187"/>
        <source>Extent of canvas:</source>
        <translation>Prostorový rozsah dat:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="190"/>
        <source>XMin:</source>
        <translation>XMin:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="177"/>
        <source>XMax:</source>
        <translation>XMax:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="108"/>
        <source>YMin:</source>
        <translation>YMin:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="164"/>
        <source>YMax:</source>
        <translation>YMax:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="268"/>
        <source>Description:</source>
        <translation>Popis:</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="275"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Composition name already exists!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Název kompozice již existuje!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="288"/>
        <source>Canvas extent</source>
        <translation>Rozsah dat</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="35"/>
        <source>Layman - Edit metadata</source>
        <translation>Layman - editovat metadata</translation>
    </message>
    <message>
        <location filename="../dlg_setPermission.ui" line="47"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="330"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="317"/>
        <source>Units:</source>
        <translation>Jednotka:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="216"/>
        <source>Scale:</source>
        <translation>Měřítko:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="255"/>
        <source>User:</source>
        <translation>Uživatel:</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="35"/>
        <source>Layman - Export Layer to server</source>
        <translation>Layman - Export vrstvy na server</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="57"/>
        <source>Export layer</source>
        <translation>Exportovat vrstvu</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="367"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="41"/>
        <source>Layman -Manage Maps</source>
        <translation>Layman - Správce mapových kompozic</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="66"/>
        <source>Layers</source>
        <translation>Vrstvy</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="78"/>
        <source>Existing layers in map composition:</source>
        <translation>Existující vrstvy v mapové kompozici:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="91"/>
        <source> Delete Layer</source>
        <translation>Smazat vrstvu</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="124"/>
        <source>Layer preview:</source>
        <translation>Náhled vrstvy:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="137"/>
        <source>Processing data:</source>
        <translation>Probíhá nahrávání:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="305"/>
        <source> Add Layer</source>
        <translation>Přidat vrstvu</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="218"/>
        <source>Loaded vector layer from canvas:</source>
        <translation type="obsolete">Vektorové vrstvy v QGIS:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="239"/>
        <source>Existing layers in Layman:</source>
        <translation>Existující vrstvy na serveru:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="252"/>
        <source>Up</source>
        <translation>Nahoru</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="265"/>
        <source>Down</source>
        <translation>Dolů</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="278"/>
        <source>Save order</source>
        <translation>Uložit pořadí</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="382"/>
        <source>Map compositions</source>
        <translation>Mapové kompozice:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="407"/>
        <source>Existing map compositions:</source>
        <translation>Existující mapové kompozice:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="420"/>
        <source> Delete Map</source>
        <translation>Smazat mapu</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="437"/>
        <source> Add Map</source>
        <translation>Přidat Mapu</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="450"/>
        <source>  Edit metada</source>
        <translation>Upravit metadata</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="463"/>
        <source>Load Map (WMS)</source>
        <translation>Načíst kompozici (WMS)</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="486"/>
        <source>Load Map (WFS)</source>
        <translation>Načíst kompozici (WFS)</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="35"/>
        <source>Layman - Add Map</source>
        <translation>Layman - Načíst mapovou komozici</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="47"/>
        <source>Load map (WMS)</source>
        <translation>Načíst kompozici (WMS)</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="127"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Unable to load already loaded composition!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Nelze načíst již načtenou kompozici!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="140"/>
        <source>Load map (WFS)</source>
        <translation>Načíst kompozici (WFS)</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="41"/>
        <source>Layman username:</source>
        <translation>Layman uživatel:</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="54"/>
        <source>Liferay username:</source>
        <translation>Liferay uživatel:</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="170"/>
        <source>Logout</source>
        <translation>Odhlásit se</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="153"/>
        <source>Loading data</source>
        <translation>Načítání dat</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="222"/>
        <source> Add to composite</source>
        <translation>Přidat do kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="199"/>
        <source>QGIS map list</source>
        <translation>Mapové vrstvy v QGIS</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="322"/>
        <source>WMS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="174"/>
        <source>WMS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="187"/>
        <source>WFS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="185"/>
        <source>Load composition</source>
        <translation>Načíst kompozici</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="119"/>
        <source>Plugin version:</source>
        <translation>Verze pluginu</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="236"/>
        <source>Set permissions</source>
        <translation>Nastavit práva</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="170"/>
        <source>Install dependencies</source>
        <translation type="obsolete">Instalovat závislosti</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="145"/>
        <source>Update</source>
        <translation>Aktualizovat</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="158"/>
        <source>Available version:</source>
        <translation>Dostupná verze:</translation>
    </message>
    <message>
        <location filename="../dlg_setPermission.ui" line="35"/>
        <source>Layman - Set permissions</source>
        <translation>Layman - Nastavení práv</translation>
    </message>
    <message>
        <location filename="../dlg_setPermission.ui" line="86"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../dlg_setPermission.ui" line="112"/>
        <source>Remove</source>
        <translation>Odebrat</translation>
    </message>
    <message>
        <location filename="../dlg_setPermission.ui" line="135"/>
        <source>Users:</source>
        <translation>Uživatelé:</translation>
    </message>
    <message>
        <location filename="../dlg_setPermission.ui" line="168"/>
        <source>Read:</source>
        <translation>Čtení:</translation>
    </message>
    <message>
        <location filename="../dlg_setPermission.ui" line="181"/>
        <source>Write:</source>
        <translation>Zápis:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="328"/>
        <source>Extent from layers</source>
        <translation type="obsolete">Rozsah z vrstev</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="499"/>
        <source> Set permissions</source>
        <translation>Nastavit práva</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="79"/>
        <source>Workspace</source>
        <translation>Pracovní prostor</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="84"/>
        <source>Permissions</source>
        <translation>Práva</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="214"/>
        <source>Show only my layers</source>
        <translation>Zobrazit jenom mé vrstvy</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="259"/>
        <source>Filter:</source>
        <translation>Filtr</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="362"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="74"/>
        <source>Compositions</source>
        <translation>Kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="198"/>
        <source>Delete map</source>
        <translation>Smazat kompozici</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="246"/>
        <source>Show only my compositions</source>
        <translation>Zobrazit jenom mé kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="197"/>
        <source>Layman version:</source>
        <translation>Verze Laymana:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="29"/>
        <source>Layman - Current composition</source>
        <translation>Layman - Aktuální kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="41"/>
        <source>Save layers</source>
        <translation>Uložit a zavřít</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="64"/>
        <source>Loaded composition:</source>
        <translation type="obsolete">Načtená kompozice:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="77"/>
        <source>New</source>
        <translation>Nová kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="90"/>
        <source>Composition is read only</source>
        <translation>Kompozice je pouze ke čtení</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="157"/>
        <source>Continue without login</source>
        <translation>POKRAČOVAT BEZ PŘIHLÁŠENÍ</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="73"/>
        <source>Extent of composition</source>
        <translation>Prostorový rozsah</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="85"/>
        <source>From canvas</source>
        <translation>Z plátna</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="151"/>
        <source>From capatibilites</source>
        <translation>Z vrstev</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="204"/>
        <source>Metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="103"/>
        <source>  Edit metadata</source>
        <translation>Upravit metadata</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="116"/>
        <source>Save changes</source>
        <translation>Uložit změny</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="272"/>
        <source>Not logged</source>
        <translation type="obsolete">Nepřihlášený uživatel</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="161"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="210"/>
        <source>Layers:</source>
        <translation>Vrstvy:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="223"/>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="272"/>
        <source>Not logged user</source>
        <translation type="obsolete">Nepřihlášený uživatel</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="249"/>
        <source>Check all layers</source>
        <translation>Vybrat vše</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="262"/>
        <source>Debug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="281"/>
        <source>Server type</source>
        <translation type="unfinished">Služba</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="286"/>
        <source>Action</source>
        <translation>Akce</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="272"/>
        <source>Anonymous</source>
        <translation type="obsolete">Nepřihlášený uživatel</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="275"/>
        <source>Get URL</source>
        <translation type="obsolete">URL Vrstvy</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="300"/>
        <source>Exporting raster</source>
        <translation>Export rastru</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../dlg_addMap.ui" line="272"/>
        <source>Nepřihlášený uživatel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="275"/>
        <source>Layer URL (copy to clipboard)</source>
        <translation>URL vrstvy (uložit do schránky)</translation>
    </message>
</context>
<context>
    <name>Layman</name>
    <message>
        <location filename="../Layman.py" line="5154"/>
        <source>&amp;Layman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Layman.py" line="318"/>
        <source>Current Row Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Layman.py" line="324"/>
        <source>Login</source>
        <translation>Přihlásit se</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="332"/>
        <source>Save as to JSON and QML</source>
        <translation type="obsolete">Uložit jako JSON a QML</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="342"/>
        <source>Load from JSON</source>
        <translation>Načíst z JSON</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="351"/>
        <source>Export layer to server</source>
        <translation>Exportovat vrstvu na server</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="358"/>
        <source>Load layer from server</source>
        <translation>Načíst vrstvu ze serveru</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="372"/>
        <source>Load map from server</source>
        <translation>Načíst mapu ze serveru</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="396"/>
        <source>Current composition</source>
        <translation>Aktuální kompozice</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="403"/>
        <source>User info</source>
        <translation>Informace o uživateli</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="333"/>
        <source>Save as to JSON and SLD</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
